AUTHOR = 'Jim Fuller'
SITENAME = 'jim.fuller.name'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Rome'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
         )

# Social widget
SOCIAL = (('linkedin', 'https://www.linkedin.com/in/jimfuller/'),
         ('twitter', 'https://twitter.com/_james_fuller'),
         ('gitlab', 'https://gitlab.com/jimfuller'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True